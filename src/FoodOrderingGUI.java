import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingGUI {
    private JPanel panel1;
    private JButton button1;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JLabel Total;
    private JButton button2;

    int total_price=0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingGUI");
        frame.setContentPane(new FoodOrderingGUI().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void confirm_order(String food,int price){
            int confirmation = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like to order "+food+"?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            if(confirmation==0){
                add_order(food,price);
            }
    }
    void add_order(String food,int price){

            String currentText = textPane1.getText();
            textPane1.setText(currentText+food+"  "+price+" yen\n");
            total_price+=price;
            Total.setText("Total    "+total_price+" yen");
            JOptionPane.showMessageDialog(null,"" +
                    "Thank you for ordering "+food+"! It will be served as soon as possible.");

    };
    void checkout(){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to checkout?",
                "Checkout Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation==0){
            JOptionPane.showMessageDialog(null,
                    "Thank you.The total price is "+total_price+" yen.");
                total_price=0;
                textPane1.setText("");
                Total.setText("Total    0 yen");
        }
    }

    public FoodOrderingGUI() {
        button1.addActionListener(new ActionListener()
                                  {public void actionPerformed(ActionEvent e) {
                                      confirm_order("Chicken", 100);
                                  }
                                  }
        );
        button1.setIcon(new ImageIcon(this.getClass().getResource(
                "chicken.png"))
        );

        button2.addActionListener(new ActionListener()
                                  {public void actionPerformed(ActionEvent e) {
                                          confirm_order("Curry", 200);
                                      }
                                  }
        );
        button2.setIcon(new ImageIcon(this.getClass().getResource(
                "curry.png"))
        );

        button3.addActionListener(new ActionListener()
                                  {public void actionPerformed(ActionEvent e) {
                                      confirm_order("Gyoza", 300);
                                  }
                                  }
        );
        button3.setIcon(new ImageIcon(this.getClass().getResource(
                "gyoza.png"))
        );

        button4.addActionListener(new ActionListener()
                                  {public void actionPerformed(ActionEvent e) {
                                      confirm_order("Ikura", 400);
                                  }
                                  }
        );
        button4.setIcon(new ImageIcon(this.getClass().getResource(
                "Ikura.png"))
        );

        button5.addActionListener(new ActionListener()
                                  {public void actionPerformed(ActionEvent e) {
                                      confirm_order("Spaghetti", 500);
                                  }
                                  }
        );
        button5.setIcon(new ImageIcon(this.getClass().getResource(
                "pasta.png"))
        );


        button6.addActionListener(new ActionListener()
                                  {public void actionPerformed(ActionEvent e) {
                                      confirm_order("Pizza", 600);
                                  }
                                  }
        );
        button6.setIcon(new ImageIcon(this.getClass().getResource(
                "pizza.jpg"))
        );



        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkout();
            }
        });



    }
}
